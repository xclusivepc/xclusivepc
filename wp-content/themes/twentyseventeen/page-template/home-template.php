<?php
/**
* Template Name: Home Template
**/
?>
<?php get_header(); ?>

<?php if( have_rows('slider') ): ?>

	<div class="slider__container">
		<div class="slider__activation__wrap owl-carousel owl-theme"> 
		<!-- Start Single Slide -->

	<?php while( have_rows('slider') ): the_row(); 

		// vars
		$image 				= get_sub_field('slide_image');
		$slide_title 		= get_sub_field('slide_title');
		$slide_description 	= get_sub_field('slide_description');
		$slide_button 		= get_sub_field('slide_button');
		?>
		 <!-- Start Single Slide -->
		<div class="slide slider__fixed--height slide__align--center" style="background: rgba(0, 0, 0, 0) url(<?php echo $image ?>) no-repeat scroll 0 0 / cover;" data--black__overlay="6">
		  <div class="container">
			<div class="row">
			  <div class="col-md-12 col-lg-12">
				<div class="slider__inner">
				  <h1><?php echo $slide_title; ?></h1>
				  <p><?php echo $slide_description; ?></p>
				  <div class="slider__btn"> <a class="htc__btn" href="<?php echo $slide_button['url']; ?>"><?php echo $slide_button['title']; ?></a> </div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="htc__slider__fornt__img"> <img src="<?php echo get_template_directory_uri(); ?>/images/slider/fornt-img/1.png" alt="slider images"> </div>
		</div>
		<!-- End Single Slide --> 
	<?php endwhile; ?>
	</ul>
    <?php endif; ?>
 <!-- End Single Slide --> 
  </div>
</div>
<!-- Start Slider Area --> 


<!-- Start Testimonial Area -->
<?php
$about_us_title 			= get_field('about_us_title');
$about_us_desc 				= get_field('about_us_desc');
$contact_form_title 		= get_field('contact_form_title');
$contact_form_sub_title 	= get_field('contact_form_sub_title');
$contact_form_short_code	= get_field('contact_form_short_code');
?>
<section class="htc__testimonial__area ptb--100" >
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12">
        <div class="htc__testimonial__wrap">
          <div class="section__title text-left">
            <h2 class="title__line"><?php echo $about_us_title; ?></h2>
            <?php echo $about_us_desc; ?>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6  col-lg-offset-1 col-sm-12 col-xs-12 smt-40">
        <div class="reguest__quote">
          <div class="section__title text-left">
		    <?php if(!empty($contact_form_title)){ ?>
				<h2 class="title__line"><?php echo $contact_form_title; ?></h2>
			<?php } ?>
			<?php if(!empty($contact_form_sub_title)){ ?>
				<p><?php echo $contact_form_sub_title; ?></p>
			<?php } ?>
          </div>
		  <?php if(!empty($contact_form_short_code)){ ?>
			<?php echo do_shortcode($contact_form_short_code); ?> 
		  <?php } ?>
			<!--
            <form id="contact-form" action=" " method="post">
              <div class="clint__comment__form">
                <div class="single__cl__form">
                  <input name="name" type="text" placeholder="Name">
                  <input name="email" type="email" placeholder="Email">
                </div>
                <div class="single__cl__form">
                  <input name="telephone" type="tel" placeholder="Phone">
                  <input name="subject" type="text" placeholder="Company">
                </div>
                <div class="single__cl__message">
                  <textarea name="message" placeholder="Massage"></textarea>
                </div>
                <div class="clint__submit__btn">
                  <button class="submit htc__btn" type="submit"> Submit </button>
                </div>
              </div>
            </form>
            <p class="form-messege"></p>			--> 
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Testimonial Area --> 
<!-- Start call_acctoin Area -->
<?php
$lets_talk_bg_image 		= get_field('lets_talk_bg_image');
$lets_talk_title 			= get_field('lets_talk_title');
$lets_talk_sub_title		= get_field('lets_talk_sub_title');
$lets_talk_button_lable		= get_field('lets_talk_button_lable');
$lets_talk_button_link 		= get_field('lets_talk_button_link');
?>
<section class="call_acctoin" style="background: rgba(0, 0, 0, 0) url(<?php echo $lets_talk_bg_image; ?>) no-repeat scroll 0 0 / cover;" data--black__overlay="7">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="call_div">
		  <?php if(!empty($lets_talk_title)){ ?>
			<h3><?php echo $lets_talk_title; ?></h3>
		  <?php } ?>
		  <?php if(!empty($lets_talk_sub_title)){ ?>
			<p><?php echo $lets_talk_sub_title; ?></p>
		  <?php } ?>
		  <?php if(!empty($lets_talk_button_lable) && !empty($lets_talk_button_link)){ ?>
			<div class="call_btn"> <a href="<?php echo $lets_talk_button_link; ?>" class="acction_num"><?php echo $lets_talk_button_lable; ?></a> </div>
		  <?php } ?>
		</div>
      </div>
    </div>
  </div>
</section>
<!-- End call_acctoin Area --> 
<!-- Start About Area -->
<?php
$our_services_title 			= get_field('our_services_title');
$our_services_sub_title 		= get_field('our_services_sub_title');
?>
						<!-- 'Our Services background image' -->
<section class="htc__about__area about--2 text__pos ptb--100 bg__white" style="background:url(https://veterans.utah.gov/wp-content/uploads/Services-Marketing-Social-Media-Advertising-Consultation-SEO-Analytics.png) no-repeat fixed center;">
	<div class="container">
		<div class="row">
		  <div class="col-xs-12">
			<div class="section__title text-center">
			  <h2 class="title__line"><?php echo $our_services_title; ?></h2>
			  <p><?php echo $our_services_sub_title; ?></p>
			</div>
		  </div>
		</div>
	
		<?php
		$testi_post_args = array(
			'post_status' 	 => 'publish',
			'post_type' 	 => 'services',
			'posts_per_page' => -1
		);

		$coursesPosts = new WP_Query( $testi_post_args );

		if ( $coursesPosts->have_posts() ) : 
		$count = 1;
		$i = 1;
		?>
		<div class="row mt--70">
			<?php while ( $coursesPosts->have_posts() ) : $coursesPosts->the_post(); 
			$postid = get_the_ID();
			$service_icon 		= get_field('service_icon', $postid);
			?>
			<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
			<div class="about foo">
			  <div class="about__inner">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p><?php echo wp_trim_words( get_the_content(), 20 ); ?></p>

				<!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
				<div class="about__icon"> <img src="<?php echo $service_icon; ?>" alt="<?php the_title(); ?>"> </div>
			  </div>
			  <div class="about__inner about__hober__info <?php if($i % 2 == 0){?>active<?php } ?>">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p><?php echo wp_trim_words( get_the_content(), 15 ); ?></p>
				<!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
				<div class="about__icon"> <img src="<?php echo $service_icon; ?>" alt="<?php the_title(); ?>"> </div>
			  </div>
			</div>
		  </div>
		<?php	
		if($count % 3 == 0){ ?></div><div class="row mt--50"> <?php $i = 0;  }
		$count ++ ;
		$i ++ ;
		endwhile; // end of the loop. ?> 
		</div>
	
		<?php endif;
		wp_reset_query();
		?> 
  </div>
</section>
<?php
$our_partner_title 		= get_field('our_partner_title');
$our_partners_logo 			= get_field('our_partners_logo');
?>
				<!-- Our Partners -->
<section id="partner-logo" style="margin-top: 50px;">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="section__title text-center">
          <h2 class="title__line"><?php echo $our_partner_title; ?></h2>
        </div>
      </div>
    </div>
    <div class="mt--70"></div>
  </div>
  <div class="container">
    <?php if( have_rows('our_partners_logo') ): ?>

	<?php while( have_rows('our_partners_logo') ): the_row(); 

		// vars
		$partner_logo_1 				= get_sub_field('partner_logo_1');
		$partner_logo_2 				= get_sub_field('partner_logo_2');
		$partner_logo_3 				= get_sub_field('partner_logo_3');
		?>
		<div class="row plogo-owrp">
		  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
			<div class="partner-logo"> <img src="<?php echo $partner_logo_1; ?>" alt="logo1"/> </div>
		  </div>
		  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
			<div class="partner-logo"> <img src="<?php echo $partner_logo_2; ?>" alt="logo2"/> </div>
		  </div>
		  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
			<div class="partner-logo"> <img src="<?php echo $partner_logo_3; ?>" alt="logo3"/> </div>
		  </div>
		</div>
	<?php endwhile; ?>
	
    <?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>