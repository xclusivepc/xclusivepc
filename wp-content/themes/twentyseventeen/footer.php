<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.2
 */

?>
  
	<footer class="htc__footer__area">
		<!-- Footer background image -->
    <div class="footer__top ptb--100" style="background: url('https://iotgeorgia.com/wp-content/uploads/2019/07/5g-americas-explores-how-the-iot-and-5g-will-create-our-connected-future_5d3edcae9cc63.jpeg') no-repeat scroll 0 0 / cover;" data--black__overlay="6" >
      <div class="container">
        <div class="row">
          <div class="htc__footer__wrap clearfix"> 
            <!-- Start Single Footer -->
        <?php
            $footer_about_title      = get_field('footer_about_title', 'option');
            $footer_about_des        = get_field('footer_about_des', 'option');
            $footer_quick_links     = get_field('footer_quick_links','option');
            $ftr_stay_title   = get_field('footer_stay_title', 'option');
            $ftr_social_links  = get_field('footer_social_links', 'option');
            $ftr_copyright_text  = get_field('footer_copyright_text', 'option');
            $footer_phone  = get_field('footer_phone', 'option');
            $footer_address  = get_field('footer_address', 'option');
            $footer_map  = get_field('footer_map', 'option');

        ?>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
              <div class="footer foo same_fo">
                <div class="footer__widget">
                  <h2 class="ft__title"><?php the_field('footer_about_title', 'option'); ?></h2>
                </div>
                <div class="ft__details">
                  <p><?php the_field('footer_about_des', 'option'); ?></p> 
                </div>
              </div>
            </div>
            <!-- End Single Footer --> 
            <!-- Start Single Footer -->
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
              <div class="footer quick__link foo same_fo">
                <div class="footer__widget">
                  <h2 class="ft__title"><?php the_field('footer_quick_links', 'option'); ?></h2>
                </div>
                <div class="footer__link">
                  <!-- <ul class="ft__menu">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">SERVICES </a></li>
                    <li><a href="#">CHANNEL</a></li>
                    <li><a href="#">PARTNER</a></li>
                    <li><a href="#">TEAM</a></li> -->
                    <?php
            wp_nav_menu (array(
                'theme_location'  => 'Social Links Menu',
                'menu'            => 'footer-menu',
                'container'       => '',
                'container_class' => 'footer__link',
                'container_id'    => '',
                'menu_class'      => 'ft__menu',
                'menu_id'         => ''
                ));
           
            ?>
               <!--    </ul> -->
                     <!--  <ul class="ft__menu">
                    <li><a href="#">WHY CHOOSE US</a></li>
                    <li><a href="#">TESTIMONIALS</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Term & Conditions</a></li> -->
                              <?php
            wp_nav_menu (array(
                'theme_location'  => 'Social Links Menu',
                'menu'            => 'footer-second',
                'container'       => '',
                'container_class' => 'footer__link',
                'container_id'    => '',
                'menu_class'      => 'ft__menu',
                'menu_id'         => ''
                ));
           
            ?>
                  <!-- </ul> -->
           
                </div>
              </div>
            </div>
            <!-- End Single Footer --> 
            <!-- Start Single Footer -->
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
              <div class="footer foo same_fo ">
                <div class="footer__widget">
                  <h2 class="ft__title"><?php the_field('footer_stay_title', 'option'); ?></h2>
                  <div class="footer__link_sh ">
                    <ul class="ft__menu_shear">
                      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                      </li>
                    </ul><br>
                    <ul class="ftr_des">
                    <li><?php the_field('footer_phone', 'option'); ?></li><br>
                    <li><?php the_field('footer_address', 'option'); ?></li>
                    </ul><br>
                    <p class="google_map"><?php the_field('footer_map', 'option'); ?></p>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Single Footer --> 
          </div>
        </div>
      </div>
    </div>
    <div class="copyright bg__theme">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="copyright__inner">
              <p><?php the_field('footer_copyright_text', 'option'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer Area --> 
</div>
<!-- Body main wrapper end --> 

<!-- Placed js at the end of the document so the pages load faster --> 

<!-- jquery latest version --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.12.0.min.js"></script> 
<!-- Bootstrap framework js --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
<!-- All js plugins included in this file. --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script> 
<!-- Waypoints.min.js. --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/waypoints.min.js"></script> 
<!-- Main js file that contents all jQuery plugins activation. --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<?php wp_footer(); ?>

</body>
</html>
