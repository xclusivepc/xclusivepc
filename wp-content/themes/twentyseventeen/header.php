<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Xclusive PC & IT || Managed Services IT Company</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico in the root directory -->
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

<!-- All css files are included here. -->
<!-- Bootstrap fremwork main css -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<!-- Owl Carousel  main css -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
<!-- This core.css file contents all plugings css file. -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/core.css">
<!-- Theme shortcodes/elements style -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/shortcode/shortcodes.css">
<!-- Theme main style -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
<!-- Responsive css -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>css/responsive.css">
<!-- User style -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>css/custom.css">

<!-- Modernizr JS -->
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>

<?php wp_head(); ?><?php $custom_logo_id = get_theme_mod( 'custom_logo' );$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );$blog_title = get_bloginfo(); ?>
</head>

<body>
<!-- Body main wrapper start -->
<div class="wrapper"><!-- Start Header Style -->
  <div id="header" class="htc-header"><!-- Start Header Top -->
    <div class="htc__header__top bg__cat--1">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
              <?php
            $header_help     = get_field('header_help', 'option');
            $header_home        = get_field('header_home', 'option');
            $header_contact     = get_field('header_contact','option');
            $header_sales   = get_field('header_sales ', 'option');
          ?>
            <ul class="heaher__top__left">
              <li><?php the_field('header_help', 'option'); ?></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="header__top__right">
              <ul class="login-register">
                <li><?php the_field('header_home', 'option'); ?></li>
                <li class="separator">/</li>
                <li><?php the_field('header_contact', 'option'); ?></li>
                <li class="separator">/</li>
                <li><?php the_field('header_sales', 'option'); ?></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Header Top --> 
    <!-- Start Mainmenu Area -->
    <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-7">
            <div class="logo"> <a href="index.php"><!-- <span class="pc">X</span>clusive <span class="pc">PC & IT</span><br>
              <span class="managed">Managed Services IT company</span>-->			  <?php if(!empty($image[0])){ ?>
              <img src="<?php echo $image[0]; ?>" alt="<?php echo $blog_title; ?>">			  <?php } else { ?>				<span class="pc">X</span>clusive <span class="pc">PC & IT</span><br>              <span class="managed">Managed Services IT company</span>			  <?php } ?>
              </a> 			</div>
          </div>
          <div class="col-md-8 col-sm-6 col-xs-5">
            <nav class="main__menu__nav  hidden-xs hidden-sm">
              <!-- <ul class="main__menu"> -->
                <!--  <li class="drop"><a class="active" href="index.html">HOME</a> </li>
                <li class="drop"><a href="#">SERVICES <i class="zmdi zmdi-chevron-down"></i></a>
                  <ul class="dropdown">
                    <li><a href="#">service</a></li>
                    <li><a href="#">Single service</a></li>
                    <li><a href="#">projects one</a></li>
                    <li><a href="#">projects two</a></li>
                  </ul>
                </li>
                <li class="drop"><a href="#">WHY CHOOSE US</a></li>
                <li><a href="#">TESTIMONIALS</a></li>
                <li><a href="#">CHANNEL</a></li>
                <li><a href="#">PARTNER</a></li>
                <li><a href="#">TEAM</a></li> -->
                <?php
            wp_nav_menu (array(
                'theme_location'  => 'Top Menu',
                'menu'            => 'primary',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'main__menu',
                'menu_id'         => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s nav navbar-nav nav_ul navbar-right">%3$s</ul>',
                'depth'           => 2,
                
                ));
           
            ?>
        <!--   </ul> -->
            </nav>
         <!--    <div class="mobile-menu clearfix visible-xs visible-sm">
              <nav id="mobile_dropdown">
                <ul>
                  <li class="drop"><a href="index.html">HOME</a> </li>
                  <li class="drop"><a href="#">SERVICES</a>
                    <ul class="dropdown">
                      <li><a href="#">service</a></li>
                      <li><a href="#">Single service</a></li>
                      <li><a href="#">projects one</a></li>
                      <li><a href="#">projects two</a></li>
                    </ul>
                  </li>
                  <li class="drop"><a href="#">WHY CHOOSE US</a></li>
                  <li><a href="#">TESTIMONIALS</a></li>
                  <li><a href="#">CHANNEL</a></li>
                  <li><a href="#">PARTNER</a></li>
                  <li><a href="#">TEAM</a></li>
                </ul>
              </nav>
            </div> -->
          </div>
        </div>
        <!-- <div class="mobile-menu-area"></div> -->
      </div>
    </div>
    <!-- End Mainmenu Area --> 
  </div>
</div>