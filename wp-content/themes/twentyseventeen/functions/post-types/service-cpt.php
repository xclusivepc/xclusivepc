<?php
#-----------------------------------------------------------------#
#------------- XCLUSIVEPC : SERVICES CUSTOM POST TYPE ------------#
#-----------------------------------------------------------------#

class XCLUSIVEPC_Services_CusotmPostType {
	
	public function __construct(){
		$this->register_post_type();
		$this->taxonomies();
		$this->courses_cat_default_terms();
		add_action( 'save_post', array(&$this,'services_set_default_object_terms'), 100, 2 );
	}

	public function register_post_type()
	{
		$args = array();

		// Services
		$args['post-type-services'] = array(
			'labels' => array(
				'name' => __( 'Services', 'xclusivepc' ),
				'singular_name' => __( 'Services', 'xclusivepc' ),
				'all_items' => __( 'Services', 'xclusivepc' ),
				'add_new' => __( 'Add New', 'xclusivepc' ),
				'add_new_item' => __( 'Add New Services', 'xclusivepc' ),
				'edit_item' => __( 'Edit Services', 'xclusivepc' ),
				'new_item' => __( 'New Services', 'xclusivepc' ),
				'view_item' => __( 'View Services', 'xclusivepc' ),
				'search_items' => __( 'Search Through Services Items', 'xclusivepc' ),
				'not_found' => __( 'No Services Items found', 'xclusivepc' ),
				'not_found_in_trash' => __( 'No Services Items found in Trash', 'xclusivepc' ),
				'parent_item_colon' => __( 'Parent Services :', 'xclusivepc' ),
				'menu_name' => __( 'Services', 'xclusivepc' ),
			),		  
			'hierarchical' => false,
			'menu_position' => 20,
	        'description' => __( 'Add a Service ', 'xclusivepc' ),
	        'supports' => array( 'title','thumbnail','editor'),
	        'menu_icon' =>  'dashicons-portfolio',
	        'public' => true,
	        'publicly_queryable' => true,
	        'exclude_from_search' => false,
	        'query_var' => true,
	        'rewrite' => true 
			);

		register_post_type('services', $args['post-type-services']);
	}
	
	public function taxonomies() {
		$taxonomies = array();

		$taxonomies['taxonomy-services-cat'] = array(
			'labels' => array(
				'name' => __( 'Services Categories', 'xclusivepc' ),
				'singular_name' => __( 'Services Category', 'xclusivepc' ),
				'search_items' =>  __( 'Search Services Categories', 'xclusivepc' ),
				'all_items' => __( 'All Services Categories', 'xclusivepc' ),
				'parent_item' => __( 'Parent Services Category', 'xclusivepc' ),
				'parent_item_colon' => __( 'Parent Services Category:', 'xclusivepc' ),
				'edit_item' => __( 'Edit Services Category', 'xclusivepc' ),
				'update_item' => __( 'Update Services Category', 'xclusivepc' ),
				'add_new_item' => __( 'Add New Services Category', 'xclusivepc' ),
				'new_item_name' => __( 'New Services Category Name', 'xclusivepc' ),
				'choose_from_most_used'	=> __( 'Choose from the most used Services categories', 'xclusivepc' )
			),
			'hierarchical' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'services-category' )
		);

		// Register taxonomy: name, cpt, arguments
		register_taxonomy('services-cat', array('services'), $taxonomies['taxonomy-services-cat']);
	}
	
	function courses_cat_default_terms(){
		$parent_term = term_exists( 'uncategorized', 'services-cat' ); // array is returned if taxonomy is given
		$parent_term_id = $parent_term['term_id']; // get numeric term id
		if ($parent_term == 0 && $parent_term == null) {
			wp_insert_term(
				'uncategorized', // the term 
				'services-cat', // the taxonomy
				array(
					'description'=> 'Default Category',
					'slug' => 'uncategorized',
					'parent'=> $parent_term_id
				)
			);
		}
	}

	function courses_set_default_object_terms( $post_id, $post ) {
		if ( 'publish' === $post->post_status ) {
			$defaults = array(
				'services-cat' => array( 'uncategorized' ),
			);
			$taxonomies = get_object_taxonomies( $post->post_type );
			foreach ( (array) $taxonomies as $taxonomy ) {
				$terms = wp_get_post_terms( $post_id, $taxonomy );
				if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
					wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
				}
			}
		}
	}
}

function xclusivepc_init_servicescpt() { new XCLUSIVEPC_Services_CusotmPostType(); }
add_action( 'init', 'xclusivepc_init_servicescpt' );