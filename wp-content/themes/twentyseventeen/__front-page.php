<?php get_header(); ?>
<div class="slider__container">
  <div class="slider__activation__wrap owl-carousel owl-theme"> 
    <!-- Start Single Slide -->
    <div class="slide slider__fixed--height slide__align--center" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri(); ?>/images/slider/bg/1.jpg) no-repeat scroll 0 0 / cover;" data--black__overlay="6">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="slider__inner">
              <h1>IT MANAGED SERVICES</h1>
              <p>Praesent velit tortor, condimentum non commodo non Read more</p>
              <div class="slider__btn"> <a class="htc__btn" href="#">CONTACT US</a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="htc__slider__fornt__img"> <img src="<?php echo get_template_directory_uri(); ?>/images/slider/fornt-img/1.png" alt="slider images"> </div>
    </div>
    <!-- End Single Slide --> 
    <!-- Start Single Slide -->
    <div class="slide slider__fixed--height slide__align--center" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri(); ?>/images/slider/bg/2.jpg) no-repeat scroll 0 0 / cover;" data--black__overlay="6">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="slider__inner">
              <h1>NETWORK DESIGN</h1>
              <p>Praesent faucibus libero odio, non sodales sapien suscipit vel</p>
              <div class="slider__btn"> <a class="htc__btn" href="#">CONTACT US</a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="htc__slider__fornt__img"> <img src="<?php echo get_template_directory_uri(); ?>/images/slider/fornt-img/1.png" alt="slider images"> </div>
    </div>
    <!-- End Single Slide --> 
  </div>
</div>
<!-- Start Slider Area --> 

<!-- Start Testimonial Area -->
<section class="htc__testimonial__area ptb--100" >
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12">
        <div class="htc__testimonial__wrap">
          <div class="section__title text-left">
            <h2 class="title__line">ABOUT <span class="text--theme">US</span></h2>
            <p>We are an international award-winning IT company with 6 offices across India.</p>
            <div class="abt_details">
              <p>Xclusive PC & IT is rated as one of the top web agencies in India by various industry magazines and review sites. We have a right blend of award-winning designers, expert web developers and Google certified digital marketers which make us a unique one-stop solution for hundreds of our clients, spread across 80+ countries.</p>
              <p>Xclusive PC & IT is rated as one of the top web agencies in India by various industry magazines and review sites. We have a right blend of award-winning designers, expert web developers and Google certified digital marketers which make us a unique one-stop solution for hundreds of our clients, spread across 80+ countries.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6  col-lg-offset-1 col-sm-12 col-xs-12 smt-40">
        <div class="reguest__quote">
          <div class="section__title text-left">
            <h2 class="title__line">Technology <span class="text--theme">Assessment</span></h2>
            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasur</p>
          </div>
          <?php echo do_shortcode('[contact-form-7 id="104" title="Contact form 1"]'); ?> <!--
            <form id="contact-form" action=" " method="post">
              <div class="clint__comment__form">
                <div class="single__cl__form">
                  <input name="name" type="text" placeholder="Name">
                  <input name="email" type="email" placeholder="Email">
                </div>
                <div class="single__cl__form">
                  <input name="telephone" type="tel" placeholder="Phone">
                  <input name="subject" type="text" placeholder="Company">
                </div>
                <div class="single__cl__message">
                  <textarea name="message" placeholder="Massage"></textarea>
                </div>
                <div class="clint__submit__btn">
                  <button class="submit htc__btn" type="submit"> Submit </button>
                </div>
              </div>
            </form>
            <p class="form-messege"></p>			--> 
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Testimonial Area --> 
<!-- Start call_acctoin Area -->
<section class="call_acctoin" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri(); ?>/images/slider/bg/1.jpg) no-repeat scroll 0 0 / cover;" data--black__overlay="7">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="call_div">
          <h3>Let's talk about what we can build together</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
          <div class="call_btn"> <a href="#" class="acction_num">Call us today at 973-563-6637</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End call_acctoin Area --> 
<!-- Start About Area -->
<section class="htc__about__area about--2 text__pos ptb--100 bg__white">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="section__title text-center">
          <h2 class="title__line">Our <span class="text--theme">Services</span></h2>
          <p>We are an international award-winning IT company with 6 offices across India, and offers web design, web development and digital marketing services.</p>
        </div>
      </div>
    </div>
    <div class="row mt--70">
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">Data Backup Management </a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="images/others/icon/1.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info">
            <h2><a href="#">Data Backup Management</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="images/others/icon/1.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">IT Managed Service</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/2.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info active">
            <h2><a href="#">IT Managed Service</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/2.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">Network Design</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/3.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info">
            <h2><a href="#">Network Design</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/3.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt--50">
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">Wireless Solutions</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/1.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info">
            <h2><a href="#">Wireless Solutions</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/1.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">Service Virtualization</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/2.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info active">
            <h2><a href="#">Service Virtualization</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/2.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xs-12">
        <div class="about foo">
          <div class="about__inner">
            <h2><a href="#">Colocation Hosting</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/3.png" alt="icon images"> </div>
          </div>
          <div class="about__inner about__hober__info">
            <h2><a href="#">Colocation Hosting</a></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <!-- <div class="about__btn"> <a href="#">READ MORE</a> </div>-->
            <div class="about__icon"> <img src="<?php echo get_template_directory_uri(); ?>/images/others/icon/3.png" alt="icon images"> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="partner-logo">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="section__title text-center">
          <h2 class="title__line">Our <span class="text--theme"> Partners</span></h2>
        </div>
      </div>
    </div>
    <div class="mt--70"></div>
  </div>
  <div class="container">
    <div class="row plogo-owrp">
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/1logo.png" alt="logo1"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/2logo.png" alt="logo2"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/3logo.png" alt="logo3"/> </div>
      </div>
    </div>
    <div class="row plogo-owrp">
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="https://xclusivepc.net/wp-content/uploads/2020/05/p1.png" alt="logo1"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="https://xclusivepc.net/wp-content/uploads/2020/05/p2.png" alt="logo2"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="https://xclusivepc.net/wp-content/uploads/2020/05/p3.png" alt="logo3"/> </div>
      </div>
    </div>
    <div class="row plogo-owrp">
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/2logo.png" alt="logo1"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/3logo.png" alt="logo2"/> </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 plogo-iwrp">
        <div class="partner-logo"> <img src="http://xclusivepc.net/wp-content/uploads/2020/05/1logo.png" alt="logo3"/> </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
